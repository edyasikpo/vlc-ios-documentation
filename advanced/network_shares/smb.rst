.. _smb:

###
SMB
###

The Server Message Block (SMB) is a file server protocol that enables you to communicate with remote servers to use their resources or share, 
open, and edit their files. Read more about SMB in this `article <https://en.wikipedia.org/wiki/Server_Message_Block>`_. 

In order to acccess media files from any device directly from VLC, you need to turn that device into an SMB server. For instance, if you wanted to access media files from a Windows or Mac computer, you need to set up an SMB server on it by enabling file sharing as explained below. 

**********************************
How to set up an SMB server on Mac
**********************************

1. Click on the **Mac** icon at the top-right corner of your computer and select **System Preferences**. 

2. On the **System Preferences** pane, click on the :guilabel:`Sharing` icon.

.. figure::  /images/advanced/networkshares/smb/sharing_pane.png
   :align:   center

3. Select :guilabel:`File Sharing` and click on :guilabel:`Options`. 

.. figure::  /images/advanced/networkshares/smb/options.png
   :align:   center

4. On the **Options** pane, select :guilabel:`Share files and folders using SMB`.

.. figure::  /images/advanced/networkshares/smb/share_files.png
   :align:   center

5. Select your user account and add your password to authenticate. 

.. figure::  /images/advanced/networkshares/smb/account_password.png
   :align:   center

.. note:: You will be required to enter the name of this user account and password when connecting VLC to your computer's SMB server. 

6. Finally, click on :guilabel:`Done`.

**************************************
How to set up an SMB server on Windows 
**************************************

Unlike the Mac OS, you need to enable SMB first before activating file sharing on Windows. Here's how to enable SMB on Windows:

1. Open the :guilabel:`Control Panel` application on your Windows computer.
2. In the Control Panel app, click on :guilabel:`Programs` on the left panel and under **Programs and Features**, click on :guilabel:`Turn Windows features on or off`.

.. figure::  /images/advanced/networkshares/smb/windows_features.PNG
   :align:   center

3. After the list of Windows features has populated, scroll down to the **SMB 1.0/CIFS File Sharing Support feature**. Check the boxes next to :guilabel:`SMB 1.0/CIFS Client` and :guilabel:`SMB 1.0/CIFS Server` to enable both features. 

.. figure::  /images/advanced/networkshares/smb/windows_smb_support.png
   :align:   center

4. On doing that, a popup window with the message "Your system needs to be restarted for the changes to be effected" will be displayed on your screen. Click on the :guilabel:`Restart` button to restart your computer.

.. note:: After enabling SMB, you need to enable file sharing as well. 

Follow the steps below to enable file sharing on Windows:

1. Open the :guilabel:`Control Panel` application on your Windows computer.
2. Once the Control Panel opens, click on :guilabel:`View network status and tasks link` under the **Network and Internet** header as seen in the image below.

.. figure::  /images/advanced/networkshares/smb/network_and_internet.png
   :align:   center

3. Click on the :guilabel:`Change advanced sharing settings link` in the left pane of the Windows and Sharing Center window.

.. figure::  /images/advanced/networkshares/smb/change_advnaced_sharing.png
   :align:   center

4. In the Advanced sharing settings window, click on the :guilabel:`Turn on network discovery` radio button and deselect the *Turn on automatic setup of network connected devices* option. 

.. figure::  /images/advanced/networkshares/smb/different_network_profiles.png
   :align:   center

5. Under File and Printer sharing, click on the :guilabel:`Turn on file and printer sharing` radio button.

6. The previously grayed Save changes button then becomes active. Click on it to save the changes. You may be prompted to enter your administrator password to save these changes.

********************************************
How to stream media files from an SMB server
********************************************

After turning your computer into an SMB file server as explained above, you can now stream media (video and audio) directly from your VLC. 

Here's how to stream media files from an SMB file server protocol:

1. Open VLC on your iPhone and tap on :guilabel:`Network`.

2. If you enabled file sharing via SMB on your computer as explained above, the server will be detected and displayed on VLC as seen in the image below. Tap on the detected SMB file server.

.. figure::  /images/advanced/networkshares/smb/network_home.jpeg
   :align:   center
   :width: 30%

3. Tap on :guilabel:`Connect` at the top right-corner of your mobile screen. 

.. figure::  /images/advanced/networkshares/smb/connect.jpeg
   :align:   center
   :width: 30%

4. Add your **user account name** and **password** of your computer. 

.. figure::  /images/advanced/networkshares/smb/authentication.jpeg
   :align:   center
   :width: 30%

5. Tap on :guilabel:`Login` to access the media files.

If you followed the steps above correctly, VLC will now have access to the media files on your computer. Select your preferred media (video or audio) and enjoy. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/advanced/networkshares/smb/smb_media_files.jpeg

   .. container:: descr

      .. figure::  /images/advanced/networkshares/smb/smb_music.jpeg


.. note:: If an SMB file server is not detected, you can add it manually by tapping on :menuselection:`Network --> Connect`. 