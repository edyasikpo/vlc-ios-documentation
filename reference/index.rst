##################
 Reference Manual
##################

This section will describe (almost) exhaustively all the features and modules of VLC.

.. toctree::
   :maxdepth: 2

   os_compatibility.rst
