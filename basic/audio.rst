.. _audio:

#####
Audio
#####

VLC for iOS can be used to play several audio formats such as .asf, .avi, .divx, .dv, .mxf, .ogg, .gm, .ps, .ts, .vob, 
and .wmv.

.. figure::  /images/basic/audio/audio_homepage.png
   :align:   center

When a new audio track is added, VLC will checks its metadata in order to extract and organize it to correctly. 

.. figure::  /images/basic/audio/album_explanation.png
   :align:   center

The audio files are organized based on the name of the artist, the album of the audio file, name of the song and genre. 

.. figure::  /images/basic/audio/audio_tabs.png
   :align:   center


Adding audio files to VLC
*************************

To add an audio file to VLC for iOS, use any of the :doc:`/gettingstarted/media_synchronization` methods. 
  
Sorting audio files
*******************

In VLC, audio files are sorted in alphanumerical order by default. However, you can change the sorting format into any of the following order:

* Descending Order.
* Album.
* Duration.
* Release date.

To sort audio files, tap on the :guilabel:`Sorting` icon and select the sorting order. 

.. figure::  /images/basic/audio/sorting_audio_files.png
   :align:   center
   :width: 30%

Sharing audio files
*******************

Sharing audio files can be achieved seamlessly on VLC for iOS. Follow the steps listed below to share audio files with your friends and family. 

1. Tap on the :guilabel:`Edit` icon button at the top-right corner of your screen.
2. Select the audio files or albums you want to share. 
3. Tap on the :guilabel:`Share` icon and select your preferred sharing platform. 
4. Tap on :guilabel:`Done` to complete the file sharing process. 

Renaming audio files
********************

If you ever want to rename an audio file, follow these steps.

1. Tap on the music album that contains the audio file you want to rename.
2. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
3. Select the audio files you want to rename. 
4. Tap on the :guilabel:`Rename` icon. 
5. Change the audio file name and tap on :guilabel:`Rename` to complete the renaming process. 

Deleting audio files
*********************

Follow the steps below to delete audio files on VLC.

1. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
2. Select the audio files you want to delete. 
3. Tap on the :guilabel:`Delete` icon. 
4. On the pop-up modal on your screen, tap on :guilabel:`Delete` to confirm the selection's deletion.