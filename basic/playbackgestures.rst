.. _playbackgestures:

#################
Playback Gestures
#################

In addition to the buttons on VLC,  you can also use different gestures to control the playback of a media. 

Here’s a list of some interesting things you can do:

1. Use your two fingers to tap anywhere on the playback screen to **pause** or **play** the current media. 
2. Pinch your screen with two fingers to **minimize** the current media. 
3. Swipe to the left to **rewind** the current media by 10 seconds. 
4. Swipe to the right to **fast forward** the current media by 30 seconds.
5. Change the current **volume** by swiping vertically on the right-hand side of the playback view.
6. Adapt **screen brightness** by swiping vertically on the left-hand side of the playback view.
7. Double tap on the side to **seek/prev** 10 seconds.
8. A long tap on a media group will display all the **Edit** icons. This will help you make changes to your media groups quickly.