.. _mediagroups:

############
Media Groups
############

A media group is a set of media files grouped automatically or manually inside a folder in the video tab of VLC.

.. figure::  /images/basic/mediagroups/media_group_main_screen.png
   :align:   center

The highlighted section in the screenshot above is an example of a media group in VLC. In this case, the media group's name is called Grownish, and it contains three media files.

*****************************************
How to create a media group automatically
*****************************************

VLC automatically creates a media group for media files with a similar name. This automatic detection and grouping is only 
done based on the similarity of the media file's name. 

For instance, ``Hey123.mp4`` and ``Hey1234.mp4`` will be grouped into the same media group named ``Hey123``. However, ``123hey.mp4`` and ``Hey123.mp4`` will not be grouped into the same media group automatically because the beginning of the media file names is different. 

.. figure::  /images/basic/mediagroups/automatic_media_groups.png
   :align:   center
   :width: 60%

Now that you understand how VLC groups media files automatically, follow the steps below to try it out yourself. 

1. First, open VLC for iOS application on your mobile phone.
2. Use the Wifi Upload or any other :doc:`/gettingstarted/media_synchronization` method of your choice to sync/download media files into your VLC app.
3. For this to work, ensure that the media files you want VLC to group automatically follow the naming format explained above.
4. After moving the media files to VLC, tap on the :guilabel:`Video` icon at the bottom-left corner of your screen. If you followed the steps above, you should see a new media group containing the videos you added to VLC. 
 
************************************
How to create a media group manually
************************************

To create a media group manually, follow the steps below:

1. Open VLC for iOS application on your mobile phone.
2. Tap on the :guilabel:`Video` icon at the bottom-left corner of your screen.
3. Ensure you already have media files on VLC because that's what we intend to group manually.
4. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
5. Select the videos you want to add to the media group and tap on the :guilabel:`Add to media group` icon.
6. Tap on the :guilabel:`New media group` at the bottom of your screen, and enter a preferred name for the media group.
7. Tap on :guilabel:`Done` to complete the media group creation process. 

If you followed the steps above, you should see the newly created group in your VLC for iOS application. 

********************
Sorting media groups
********************

Media groups are sorted in alphanumerical order by default. However, you can change the sorting format into any of the following order:

* Descending Order.
* Duration.
* Insertion date.
* Last modification date.
* Number of videos.

To sort a media group, tap on the :guilabel:`Sorting` icon and select the sorting order. 

.. figure::  /images/basic/mediagroups/sorting_media_groups.png
   :align:   center
   :width: 30%

********************
Sharing media groups
********************

You can share media group's with anyone. Follow the steps below to share a media group.

1. Tap on the :guilabel:`Edit` icon button at the top-right corner of your screen.
2. Select the media group you want to share. 
3. Tap on the :guilabel:`Share` icon and select your preferred sharing platform. 
4. Tap on :guilabel:`Done` to complete the media group sharing process. 

*********************
Renaming media groups 
*********************

If you ever want to rename a media group, follow these steps.

1. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
2. Select the media file you want to rename. 
3. Tap on the :guilabel:`Rename` icon. 
4. Change the name of the media group and tap on :guilabel:`Rename` to complete the renaming process. 

*********************
Deleting media groups
*********************

Follow the steps below to delete a media group.

1. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
2. Select the media file you want to delete. 
3. Tap on the :guilabel:`Delete` icon. 
4. On the pop-up on your screen, tap on :guilabel:`Delete`.

************************************************
Other things you need to know about media groups
************************************************

This section covers other important things you need to know about media groups. 

1. You can't have an empty media group. So if you delete all the media files in an existing media group, that media group will be deleted automatically from VLC.
2. A long tap on a media group will display all the :guilabel:`Edit` icons. This will help you make changes to your media groups quickly.

.. figure::  /images/basic/mediagroups/media_group_long_tap.jpeg
   :align:   center
   :width: 30%

3. Media groups can be regrouped in alphabetical order. To do this, tap on the :guilabel:`Edit` icon at the top-right corner of your screen and tap on the :guilabel:`Regroup` icon. Then click on :guilabel:`Okay` to complete the regrouping process.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/basic/mediagroups/edit_icon.png

   .. container:: descr

      .. figure:: /images/basic/mediagroups/regroup_icon.png