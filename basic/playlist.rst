.. _playlist:

########
Playlist
########

A playlist is a list of video or audio files that can be played back on media player either sequentially or in a shuffled order. This documentation explains how you can add videos or audio files to a playlist on VLC for iOS. 

How to add audio files to a playlist
**************************************

To add audio files to a playlist on VLC for iOS, follow the steps below:

1. Open your VLC for iOS application, and tap on :guilabel:`Audio` icon at the bottom-left corner of your iPhone.

.. figure::  /images/basic/playlist/audio.jpeg
   :align:   center
   :width: 60%

2. On the :guilabel:`Audio` pane, tap on the :guilabel:`Edit` icon at the top-right corner of your iPhone.

.. figure::  /images/basic/playlist/edit_icon_audio.jpeg
   :align:   center
   :width: 30%

3. Select the audio file(s) you want to add to the playlist, and tap on :guilabel:`Add to Playlist`. 

.. figure::  /images/basic/playlist/select_audio_files.jpeg
   :align:   center
   :width: 30%

4. Enter a title for the your playlist and tap on :guilabel:`Done`. 

.. figure::  /images/basic/playlist/playlist_title_audio.jpeg
   :align:   center
   :width: 30%

5. To see your newly created playlist, tap on the :guilabel:`Playlist` icon at bottom of your iPhone. 

.. figure::  /images/basic/playlist/audio_playlist.jpeg
   :align:   center
   :width: 30%

How to add videos to a playlist
********************************

To add videos to a playlist on VLC for iOS, follow the steps below:

1. Open your VLC for iOS application, and tap on :guilabel:`Video` icon at the bottom-left corner of your iPhone.

.. figure::  /images/basic/playlist/video.jpeg
   :align:   center
   :width: 60%

2. On the :guilabel:`Video` pane, tap on the :guilabel:`Edit` icon at the top-right corner of your iPhone.

.. figure::  /images/basic/playlist/edit_icon_video.jpeg
   :align:   center
   :width: 30%

3. Select the video(s) you want to add to the playlist, and tap on :guilabel:`Add to Playlist`. 

.. figure::  /images/basic/playlist/select_videos.jpeg
   :align:   center
   :width: 30%

4. Enter a title for the your playlist and tap on :guilabel:`Done`. 

.. figure::  /images/basic/playlist/playlist_title_video.jpeg
   :align:   center
   :width: 30%

5. To see your newly created playlist, tap on the :guilabel:`Playlist` icon at bottom of your iPhone. 

.. figure::  /images/basic/playlist/video_playlist.jpeg
   :align:   center
   :width: 30%