.. _media_synchronization:

#####################
Media Synchronization
#####################

You can synchronize media files on VLC for iOS using iTunes, WiFi, cloud services, or downloading directly from the web.

*******************
iTunes file sharing
*******************

The :guilabel:`File Sharing` feature on iTunes can be used to add or delete media files to VLC for iOS. Read more about how to implement this on `Apple's documentation <http://support.apple.com/kb/HT4094>`__.

***********
WiFi upload
***********

If your iOS device and your computer are on the same local WiFi network, you can use :guilabel:`WiFi Upload` to add media files to your VLC library.
To do this, follow the steps below:

1. Ensure that your computer and iOS device are on the same network.
2. Tap the :guilabel:`Network` tab on your VLC for iOS application.

.. figure::  /images/gettingstarted/mobileoverview/ios/network_tab.png
   :align:   center
   :width: 30%

3. Tap the :guilabel:`Sharing via WiFi` toggle button to activate Wifi sharing.

.. figure::  /images/gettingstarted/mobileoverview/ios/sharing_via_wifi.jpg
   :align:   center
   :width: 30%

4. Enter the IP address on the web browser of your computer to access the :guilabel:`Sharing via WiFi` page on the computer’s browser window.

.. figure::  /images/gettingstarted/mobileoverview/ios/wifi_upload.PNG
   :align:   center
   :width: 60%

.. note:: If your VLC app is not open at this point, you won't be able to access the :guilabel:`Sharing via WiFi` page on the computer’s browser window.

5. Drag and drop files in the browser window or click on the :guilabel:`+` button to use the file picker dialogue to upload the media file(s) to your VLC for iOS mobile application. 


VLC for iOS WiFi upload supports multiple uploads at the same time and will indicate through a progress bar when the upload is complete. 
You can start the playback of any file on your iOS device as soon as they appear without waiting for the media file to finish uploading. 

**************
Cloud services
**************

In VLC for iOS, you can upload, stream, and download media files with any of the following cloud service providers: `Dropbox  <https://www.dropbox.com/>`__, `Google Drive  <https://www.google.com/drive/>`__, 
`Box  <https://account.box.com>`__, `OneDrive  <https://www.microsoft.com/en/microsoft-365/onedrive/online-cloud-storage>`__, `iCloud Drive <https://www.icloud.com/>`__.

To do this, follow the steps below:

1. Tap the :guilabel:`Network` tab on your VLC for iOS application.
2. Tap on :guilabel:`Cloud Services`.

.. figure::  /images/gettingstarted/mobileoverview/ios/cloud_services.PNG
   :align:   center
   :width: 30%

3. Select and login to your preferred cloud service platform. 

.. figure::  /images/gettingstarted/mobileoverview/ios/cloud_services_main.jpg
   :align:   center
   :width: 30%

4. Start streaming and downloading media files directly from the cloud service. 

**********************
Downloads from the web
**********************

You can download media files directly from the web. To do this, tab on :guilabel:`Network`, select :guilabel:`Downloads`, 
enter the URL address of the file you want to download and tap on the :guilabel:`Download` button. A progress bar will indicate when the download is complete.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/gettingstarted/mobileoverview/ios/downloads.jpg

   .. container:: descr

      .. figure:: /images/gettingstarted/mobileoverview/ios/downloads_main.jpg


.. Note:: Downloads from popular video playback sites such as YouTube or Vimeo are not supported in VLC for iOS.
